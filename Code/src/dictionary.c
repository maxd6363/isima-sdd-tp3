/**
 * @brief      Implémentation des fontions pour gérer le dictionaire
 *
 * @author     Maxime POULAIN - Lilian HERTEL
 * @date       2021
 */

#include "dictionary.h"

#include "bool.h"
#include "color.h"
#include "list.h"
#include "queue.h"
#include "stack.h"
#include "string.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/**
 * @brief Génère un arbre à partir d'un fichier texte, avec la notation
 * algébrique
 *
 * @param filename Le nom du fichier
 * @param out L'arbre généré
 * @return error_t Erreur possible
 */
error_t treeGenerateFromFile(char *filename, tree_t *out) {
	FILE *flot;
	error_t error = OK;
	data_tree_t dataRead;
	stack_t stack;
	tree_node_t **prec = out;
	tree_node_t *tmpDepile;

	bool pushNextValue = false;

	stackInit(&stack, STACK_SIZE);

	if (filename == NULL) {
		error = NULL_POINTER;
	} else {
		flot = fopen(filename, "r");
		if (flot == NULL) {
			error = FILE_NOT_FOUND;
		} else {
			do {
				fscanf(flot, "%c ", &dataRead);
				switch (dataRead) {
				case '(':
					pushNextValue = true;
					break;
				case '*':
					prec = &((*prec)->verti);
					break;
				case '+':
					stackPop(&stack, &tmpDepile);
					prec = &(tmpDepile->hori);
					pushNextValue = true;
					break;
				case ')':
					stackPop(&stack, &tmpDepile);
					break;
				default:
					*prec = treeNodeAlloc(dataRead);
					if (pushNextValue) {
						stackPush(&stack, *prec);
					}
					pushNextValue = false;
					break;
				}
			} while (!feof(flot));
			fclose(flot);
		}
	}
	stackFree(&stack);
	return error;
}

/**
 * @brief Ajoute un mot dans un arbre, de manière triée
 *
 * @param tree L'arbre à modifier
 * @param word Le mot à ajouter
 */
void treeAddWord(tree_t *tree, char *word) {
	int word_pos; // position de la première lettre à insérer après prec
	char sanitized_word[MAX_STRING_SIZE];
	tree_node_t **prec;
	sanitizeString(word, sanitized_word);
	printf("Ajout du mot : \"%s\"\n", sanitized_word);
	prec = treeSearch(tree, sanitized_word, &word_pos);
	
	while (sanitized_word[word_pos] != '\0') {
		addLetter(prec, sanitized_word[word_pos]);
		prec = &((*prec)->verti);
		word_pos++;
	}
	((tree_node_t *)(prec-2))->data = toUpperChar(((tree_node_t *)(prec-2))->data); 
}

/**
 * @brief Sous programme pour ajouter une lettre d'un mot dans un arbre
 *
 * @param prec Pointeur précédent où ajouter
 * @param letter La lettre à ajouter
 */
void addLetter(tree_node_t **prec, char letter) {
	tree_node_t *new = treeNodeAlloc(letter);
	new->hori = *prec;
	*prec = new;
}

/**
 * @brief Recherche un mot dans un arbre, renvoit un précédent
 *
 * @param tree L'arbre à parcourir
 * @param word Le mot à chercher
 * @param word_pos La taille du mot présent dans l'arbre
 * @return tree_node_t** Le précédent
 */
tree_node_t **treeSearch(tree_t *tree, char *word, int *word_pos) {
	tree_node_t **prec = tree;
	tree_node_t *curr = *tree;
	int wordLength = strlen(word);
	int depth = 0;
	bool found = false;

	while (curr != NULL && depth < wordLength && !found) {
		findBrother(&curr, &prec, word[depth]);
		if (curr != NULL &&
			toLowerChar(curr->data) == toLowerChar(word[depth])) {
			prec = &(curr->verti);
			curr = curr->verti;
			depth++;
		} else {
			found = true;
		}
	}
	*word_pos = depth;
	return prec;
}

/**
 * @brief Affiche tous les mots qui commencent par un motif donné
 *
 * @param tree L'arbre à parcourir
 * @param motif Le motif à chercher
 */
void treeSearchMotif(tree_t *tree, char *motif) {
	int wordPosition;
	tree_node_t **treePosition;
	treePosition = treeSearch(tree, motif, &wordPosition);

	if (*treePosition == NULL) {
		printf(RED "Aucun mot ne commence par le motif suivant : \"%s\"\n" RESET, motif);
	} else {
		printf(GRE "Mot(s) commençant par le motif suivant : \"%s\"\n" RESET, motif);
		treePrintPrefix(*treePosition, motif);
	}
}

/**
 * @brief Affiche un les mots dans un arbre
 *
 * @param tree L'arbre à afficher
 */
void treePrint(tree_t tree) { treePrintPrefix(tree, NULL); }

/**
 * @brief Affiche un les mots dans un arbre
 *
 * @param tree L'arbre à afficher
 * @param prefix Un préfixe à afficher devant chaque mot (pour la question 4)
 */
void treePrintPrefix(tree_t tree, char *prefix) {
	stack_t stack;
	bool end = false;
	tree_t root = tree;
	data_tree_t buffer[MAX_STRING_SIZE];
	int currentBufferPosition = 0;

	stackInit(&stack, STACK_SIZE);
	while (!end) {
		while (root != NULL) {
			buffer[currentBufferPosition] = toLowerChar(root->data);
			currentBufferPosition++;
			if (isCharUpper(root->data)) {
				buffer[currentBufferPosition] = '\0';
				if (prefix != NULL) {
					printf("%s", prefix);
				}
				printf("%s\n", buffer);
			}
			stackPush(&stack, root);
			root = root->verti;
		}

		if (!isStackEmpty(stack)) {
			currentBufferPosition--;
			stackPop(&stack, &root);
			root = root->hori;
		} else {
			end = true;
		}
	}
	printf("\n");
	stackFree(&stack);
}
