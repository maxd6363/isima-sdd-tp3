/**
 * @brief      Déclaration du type pile
 *
 * @author     Maxime POULAIN - Lilian HERTEL
 * @date       2021
 */

#ifndef STACK_H
#define STACK_H

#include "bool.h"
#include "error.h"
#include "tree.h"

typedef tree_t data_stack_t;

#define PILE_FORMAT "%p"

typedef struct {
	int sizeMax;
	int top;
	data_stack_t *data;
} stack_t;

/**
 * @brief Initialise la pile à zero avec sizeMax place
 *
 * @param out La pile
 * @param sizeMax La taille maximale
 * @return error_t L'erreur
 */
error_t stackInit(stack_t *out, int sizeMax);

/**
 * @brief Affiche une pile
 *
 * @param stack La pile
 */
void stackPrint(stack_t stack);

/**
 * @brief Insère une valeur dans la pile
 *
 * @param stack La pile
 * @param value La valeur
 * @return error_t L'erreur
 */
error_t stackPush(stack_t *stack, data_stack_t value);

/**
 * @brief Sort une valeur dans la pile
 *
 * @param stack La pile
 * @param value La valeur
 * @return error_t L'erreur
 */
error_t stackPop(stack_t *stack, data_stack_t *out);

/**
 * @brief Vérifie si la pile est plein
 *
 * @param stack La pile à tester
 * @return bool Le résultat
 */
bool isStackFull(stack_t stack);

/**
 * @brief Vérifie si la pile est vide
 *
 * @param stack La pile à tester
 * @return bool Le résultat
 */
bool isStackEmpty(stack_t stack);

/**
 * @brief Libère la mémoire utilisé par une pile
 *
 * @param stack La pile à libérer
 * @return error_t L'erreur
 */
error_t stackFree(stack_t *stack);

/**
 * @brief Permet de changer la taille maximal de la pile
 *
 * @param stack La pile
 * @param sizeMax La nouvelle taille max
 * @return error_t L'erreur
 */
error_t stackResize(stack_t *stack, int sizeMax);

/**
 * @brief Récupère l'élément en haut de la pile
 *
 * @param stack La pile
 * @param top L'élément en haut
 * @return error_t L'erreur
 */
error_t stackTop(stack_t stack, data_stack_t *top);

/**
 * @brief Insère une valeur dans la pile, sans aucun test préalable, plus rapide
 * mais non protégé
 *
 * @param stack La pile
 * @param value La valeur
 * @return error_t L'erreur
 */
void stackPushLight(stack_t *stack, data_stack_t value);

/**
 * @brief Sort une valeur dans la pile, sans aucun test préalable, plus rapide
 * mais non protégé
 *
 * @param stack La pile
 * @param value La valeur
 * @return error_t L'erreur
 */
void stackPopLight(stack_t *stack, data_stack_t *out);

#endif