/**
 * @brief      Définition du type file
 *
 * @author     Maxime POULAIN - Lilian HERTEL
 * @date       2021
 */

#ifndef QUEUE_H
#define QUEUE_H

#include "bool.h"
#include "error.h"
#include "tree.h"

typedef tree_t data_queue_t;

#define FILE_FORMAT "%p"

typedef struct {
	int sizeMax;
	int nbElem;
	int deb;
	int fin;
	data_queue_t *data;
} queue_t;

error_t queueInit(queue_t *out, int sizeMax);
void queuePrint(queue_t queue);
error_t queuePush(queue_t *queue, data_queue_t value);
error_t queuePull(queue_t *queue, data_queue_t *out);
error_t queueEnd(queue_t *queue, data_queue_t *end);
bool isQueueFull(queue_t queue);
bool isQueueEmpty(queue_t queue);
error_t queueFree(queue_t *queue);

#endif