/**
 * @brief      Implémentation des fontions pour gérer les chaines de caractères
 *
 * @author     Maxime POULAIN - Lilian HERTEL
 * @date       2021
 */

#include "bool.h"
#include <string.h>

/**
 * @brief Test si le caractère est en minuscule
 *
 * @param c Le caractère
 * @return true Oui
 * @return false Non
 */
bool isCharLower(char c) { return c >= 'a' && c <= 'z'; }

/**
 * @brief Test si le caractère est en majuscule
 *
 * @param c Le caractère
 * @return true Oui
 * @return false Non
 */
bool isCharUpper(char c) { return c >= 'A' && c <= 'Z'; }

/**
 * @brief Met un caractère en minuscule
 *
 * @param c Le caractère
 * @return char Le caractère en minuscule
 */
char toLowerChar(char c) {
	if (isCharUpper(c)) {
		return c + 'a' - 'A';
	}
	return c;
}

/**
 * @brief Met un caractère en majuscule
 *
 * @param c Le caractère
 * @return char Le caractère en majuscule
 */
char toUpperChar(char c) {
	if (isCharLower(c)) {
		return c - ('a' - 'A');
	}
	return c;
}

/**
 * @brief Enlève les espaces et met en minuscule
 * 
 * @param string La chaine à traiter
 * @param out La chaine retourné
 */
void sanitizeString(const char *const string, char *out) {
	int length;
	int i;
	int j = 0;
	length = strlen(string);

	for (i = 0; i < length; i++) {
		if (string[i] != ' ') {
			out[j] = toLowerChar(string[i]);
			j++;
		}
	}
	out[j]='\0';
}
