/**
 * @brief      Définition des fontions pour gérer les listes chainées
 *
 * @author     Maxime POULAIN - Lilian HERTEL
 * @date       2021
 */

#ifndef LIST_H
#define LIST_H

#include "tree.h"

/**
 * @brief Chercher dans une liste chaîné trié
 *
 * @param curr Renvoie par adresse le courant
 * @param prec Renvoie par adresse le précédent
 * @param letter La lettre à chercher
 */
void findBrother(tree_node_t **curr, tree_node_t ***prec, char letter);

#endif