/**
 * @brief      Implémentation du type file
 *
 * @author     Maxime POULAIN - Lilian HERTEL
 * @date       2021
 */

#include "queue.h"

#include <stdio.h>
#include <stdlib.h>

/**
 * @brief Initialise la file avec sizeMax places
 *
 * @param out La file
 * @param sizeMax La taille maximale
 * @return error_t Le code d'erreur
 */
error_t queueInit(queue_t *out, int sizeMax) {
	error_t error = OK;

	if (out == NULL) {
		error = NULL_POINTER;
	} else if (sizeMax < 0) {
		error = OUT_OF_BOUNDS;
	} else {
		out->sizeMax = sizeMax;
		out->nbElem = 0;
		out->deb = 0;
		out->fin = sizeMax - 1;
		out->data = (data_queue_t *)malloc(sizeMax * sizeof(data_queue_t));
		if (out->data == NULL) {
			error = ALLOC_ERROR;
			out->sizeMax = 0;
		}
	}
	return error;
}

/**
 * @brief Affiche une file
 *
 * @param queue La file
 */
void queuePrint(queue_t queue) {
	int i;
	if (isQueueEmpty(queue))
		printf("File vide.");
	else
		printf("Contenu de la file : \n");
	for (i = queue.deb; i < (queue.deb + queue.nbElem); i++) {
		printf(FILE_FORMAT " ", queue.data[i % (queue.sizeMax)]);
	}
	printf("\n\n");
}

/**
 * @brief Insère une valeur dans la file
 *
 * @param queue La file
 * @param value La valeur à insérer
 * @return error_t Le code d'erreur
 */
error_t queuePush(queue_t *queue, data_queue_t value) {
	error_t error = OK;
	if (queue == NULL) {
		error = NULL_POINTER;
	} else if (isQueueFull(*queue)) {
		error = QUEUE_OVERFLOW;
	} else {
		queue->fin = queue->fin + 1;
		queue->data[(queue->fin) % (queue->sizeMax)] = value;
		queue->nbElem = queue->nbElem + 1;
	}
	return error;
}

/**
 * @brief Enlève une valeur de la file
 *
 * @param queue La file
 * @param out La valeur récupérée
 * @return error_t Le code d'erreur
 */
error_t queuePull(queue_t *queue, data_queue_t *out) {
	error_t error = OK;
	if (queue == NULL || out == NULL) {
		error = NULL_POINTER;
	} else if (isQueueEmpty(*queue)) {
		error = QUEUE_EMPTY;
	} else {
		*out = queue->data[queue->deb % (queue->sizeMax)];
		queue->deb = queue->deb + 1;
		queue->nbElem = queue->nbElem - 1;
	}
	return error;
}

/**
 * @brief Récupère la valeur en fin de file (dernière valeur ajoutée) sans la
 * supprimer
 *
 * @param queue La file
 * @param end La valeur récupérée
 * @return error_t Le code d'erreur
 */
error_t queueEnd(queue_t *queue, data_queue_t *end) {
	error_t error = OK;
	if (queue == NULL || end == NULL) {
		error = NULL_POINTER;
	} else if (isQueueEmpty(*queue)) {
		error = QUEUE_EMPTY;
	} else {
		*end = queue->data[(queue->fin) % queue->sizeMax];
	}
	return error;
}

/**
 * @brief Vérifie si la fqueue_t queueile est pleine
 *
 * @param queue La file à tester
 * @return bool Le résultat
 */
bool isQueueFull(queue_t queue) { return queue.nbElem >= queue.sizeMax; }

/**
 * @brief Vérifie si la file est vide
 *
 * @param queue La file à tester
 * @return bool Le résultat
 */
bool isQueueEmpty(queue_t queue) { return queue.nbElem <= 0; }

/**
 * @brief Libère la mémoire utilisée par une file
 *
 * @param stack La file à libérer
 * @return error_t Le code d'erreur
 */
error_t queueFree(queue_t *queue) {
	if (queue != NULL) {
		queue->sizeMax = 0;
		queue->nbElem = 0;
		queue->deb = 0;
		queue->fin = 0;
		if (queue->data != NULL) {
			free(queue->data);
		}
	}
	return OK;
}
