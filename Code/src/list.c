/**
 * @brief      Implémentation des fontions pour gérer les listes chainées
 *
 * @author     Maxime POULAIN - Lilian HERTEL
 * @date       2021
 */

#include "list.h"

#include <stdlib.h>

#include "string.h"

/**
 * @brief Chercher dans une liste chaîné trié
 *
 * @param curr Renvoie par adresse le courant
 * @param prec Renvoie par adresse le précédent
 * @param letter La lettre à chercher
 */
void findBrother(tree_node_t **curr, tree_node_t ***prec, char letter) {

	tree_node_t **precL = *prec;
	tree_node_t *currL = *curr;

	while (currL != NULL && toLowerChar(currL->data) < toLowerChar(letter)) {
		precL = &(currL->hori);
		currL = currL->hori;
	}
	*curr = currL;
	*prec = precL;
}