/**
 * @brief      Définition des fontions pour gérer le dictionaire
 *
 * @author     Maxime POULAIN - Lilian HERTEL
 * @date       2021
 */

#ifndef DICTIONARY_H
#define DICTIONARY_H

#include "error.h"
#include "tree.h"

/**
 * @brief Génère un arbre à partir d'un fichier texte, avec la notation
 * algébrique
 *
 * @param filename Le nom du fichier
 * @param out L'arbre généré
 * @return error_t Erreur possible
 */
error_t treeGenerateFromFile(char *filename, tree_t *out);

/**
 * @brief Ajoute un mot dans un arbre, de manière triée
 *
 * @param tree L'arbre à modifier
 * @param word Le mot à ajouter
 */
void treeAddWord(tree_t *tree, char *word);

/**
 * @brief Sous programme pour ajouter une lettre d'un mot dans un arbre
 *
 * @param prec Pointer précédent où ajouter
 * @param letter La lettre à ajouter
 */
void addLetter(tree_node_t **prec, char letter);

/**
 * @brief Recherche un mot dans un arbre, renvoit un précédent
 *
 * @param tree L'arbre à parcourir
 * @param word Le mot à chercher
 * @param word_pos La taille du mot présent dans l'arbre
 * @return tree_node_t** Le précédent
 */
tree_node_t **treeSearch(tree_t *tree, char *word, int *word_pos);

/**
 * @brief Affiche tous les mots qui commencent par un motif donné
 *
 * @param tree L'arbre à parcourir
 * @param motif Le motif à chercher
 */
void treeSearchMotif(tree_t *tree, char *motif);

/**
 * @brief Affiche un les mots dans un arbre
 *
 * @param tree L'arbre à afficher
 */
void treePrint(tree_t tree);

/**
 * @brief Affiche un les mots dans un arbre
 *
 * @param tree L'arbre à afficher
 * @param prefix Un préfixe à afficher devant chaque mot (pour la question 4)
 */
void treePrintPrefix(tree_t tree, char *prefix);

#endif