/**
 * @brief      Définition des fontions pour gérer les arbres
 *
 * @author     Maxime POULAIN - Lilian HERTEL
 * @date       2021
 */

#ifndef TREE_H
#define TREE_H

#define STACK_SIZE 1024
#define QUEUE_SIZE 1024

#include "error.h"

typedef char data_tree_t;

typedef struct internal_node_t {
	data_tree_t data;
	struct internal_node_t *hori;
	struct internal_node_t *verti;
} tree_node_t, *tree_t;

/**
 * @brief Initialiste un arbre
 *
 * @param tree L'arbre à initialisé, par adresse
 */
void treeInit(tree_t *tree);

/**
 * @brief Affiche un arbre brut, pour débuguer
 *
 * @param tree L'arbre à afficher
 */
void treeDebugPrint(tree_t tree);

/**
 * @brief Libère la place mémoire prise par un arbre
 *
 * @param tree L'arbre à libérer
 */
void treeFree(tree_t *tree);

/**
 * @brief Alloue un maillon de l'arbre
 *
 * @param value La valeur du maillon
 * @return tree_node_t* L'adresse du maillon crée, NULL si échec
 */
tree_node_t *treeNodeAlloc(data_tree_t value);

#endif
