/**
 * @brief      Implémentation des fontions pour gérer les arbres
 *
 * @author     Maxime POULAIN - Lilian HERTEL
 * @date       2021
 */

#include "tree.h"

#include "bool.h"
#include "queue.h"
#include "stack.h"
#include "string.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/**
 * @brief Initialiste un arbre
 *
 * @param tree L'arbre à initialisé, par adresse
 */
void treeInit(tree_t *tree) {
	if (tree != NULL) {
		*tree = NULL;
	}
}

/**
 * @brief Affiche un arbre brut, pour débuguer
 *
 * @param tree L'arbre à afficher
 */
void treeDebugPrint(tree_t tree) {
	stack_t stack;
	bool end = false;
	tree_t root = tree;
	int depth = 0;

	stackInit(&stack, STACK_SIZE);
	while (!end) {
		while (root != NULL) {
			for (int i = 0; i < depth; i++) {
				printf("│  ");
			}
			printf("├─── %c\n", root->data);
			stackPush(&stack, root);
			root = root->verti;
			depth++;
		}
		if (!isStackEmpty(stack)) {
			stackPop(&stack, &root);
			root = root->hori;
			depth--;
		} else {
			end = true;
		}
	}
	printf("\n");
	stackFree(&stack);
}

/**
 * @brief Libère la place mémoire prise par un arbre
 *
 * @param tree L'arbre à libérer
 */
void treeFree(tree_t *tree) {
	stack_t stack;
	bool end = false;
	tree_t root = *tree;
	tree_t tmp;
	stackInit(&stack, STACK_SIZE);
	while (!end) {
		while (root != NULL) {
			stackPush(&stack, root);
			root = root->verti;
		}
		if (!isStackEmpty(stack)) {
			stackPop(&stack, &root);
			tmp = root;
			root = root->hori;
			free(tmp);

		} else {
			end = true;
		}
	}
	stackFree(&stack);
	*tree = NULL;
}

/**
 * @brief Alloue un maillon de l'arbre
 *
 * @param value La valeur du maillon
 * @return tree_node_t* L'adresse du maillon crée, NULL si échec
 */
tree_node_t *treeNodeAlloc(data_tree_t value) {
	tree_node_t *node = (tree_node_t *)malloc(sizeof(tree_node_t));
	if (node != NULL) {
		node->data = value;
		node->hori = NULL;
		node->verti = NULL;
	}
	return node;
}