/**
 * @brief      Point d'entrée du programme
 *
 * @author     Maxime POULAIN - Lilian HERTEL
 * @date       2021
 */

#include <stdio.h>
#include <stdlib.h>

#include "test.h"

#include "tree.h"
#include "dictionary.h"

/**
 * 
 * @brief      Point d'entrée du programme
 *
 * @return     Code d'erreur du programme
 */
int main(void)
{
	testRun();
	return 0;
}