#ifndef TEST_H
#define TEST_H

#include "bool.h"

/**
 * @brief Test le chargement de formes algébriques
 * 
 */
void testFormeAlgebrique(void);

/**
 * @brief Test de l'affichage d'arbres
 * 
 */
void testAffichage(void);

/**
 * @brief Test d'insertion de mots dans un arbre
 * 
 */
void testInsertion(void);

/**
 * @brief Test de recherche de motif de mot
 * 
 */
void testRechercheMotif(void);

/**
 * @brief Lance les tests
 * 
 */
void testRun(void);

/**
 * @brief Affiche un ficher text, pour afficher la forme algébrique
 * 
 * @param filename Nom du fichier
 */
void testAfficherFichier(char *filename);

/**
 * @brief Un arbre simple pour une visualisation avec DDD
 * 
 */
void testDDD(void);

#endif