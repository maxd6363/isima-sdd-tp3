/**
 * @brief      Définition des fontions pour gérer les chaines de caractères
 *
 * @author     Maxime POULAIN - Lilian HERTEL
 * @date       2021
 */

#ifndef STRING_H
#define STRING_H

#include "bool.h"

#define MAX_STRING_SIZE 128

/**
 * @brief Test si le caractère est en minuscule
 *
 * @param c Le caractère
 * @return true Oui
 * @return false Non
 */
bool isCharLower(char c);

/**
 * @brief Test si le caractère est en majuscule
 *
 * @param c Le caractère
 * @return true Oui
 * @return false Non
 */
bool isCharUpper(char c);

/**
 * @brief Met un caractère en minuscule
 *
 * @param c Le caractère
 * @return char Le caractère en minuscule
 */
char toLowerChar(char c);

/**
 * @brief Met un caractère en majuscule
 *
 * @param c Le caractère
 * @return char Le caractère en majuscule
 */
char toUpperChar(char c);

/**
 * @brief Enlève les espaces et met en minuscule
 * 
 * @param string La chaine à traiter
 * @param out La chaine retourné
 */
void sanitizeString(const char * const string, char *out);

#endif