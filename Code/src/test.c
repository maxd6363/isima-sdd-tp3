#include "test.h"

#include <stdio.h>
#include <stdlib.h>

#include "color.h"
#include "dictionary.h"
#include "tree.h"

/**
 * @brief Test le chargement de formes algébriques
 *
 */
void testFormeAlgebrique(void)
{
    tree_t tree;
    error_t error;
    treeInit(&tree);

    printf(GRE "\n\n\n============= Tests forme algébrique =============\n" RESET);

    // Un arbre vide
    error = treeGenerateFromFile("data/tests/arbreVide.dat", &tree);
    printf("Arbre vide : \n");
    testAfficherFichier("data/tests/arbreVide.dat");
    error == OK ? treeDebugPrint(tree) : logError(error);
    treeFree(&tree);
    treeInit(&tree);

    // Un arbre avec une seule lettre
    error = treeGenerateFromFile("data/tests/arbreUneSeuleLettre.dat", &tree);
    printf("Arbre avec une seule lettre : \n");
    testAfficherFichier("data/tests/arbreUneSeuleLettre.dat");
    error == OK ? treeDebugPrint(tree) : logError(error);
    treeFree(&tree);
    treeInit(&tree);

    // Un arbre avec que des liens horizontaux (liste chaînée)
    error = treeGenerateFromFile("data/tests/arbreLienHorizontaux.dat", &tree);
    printf("Arbre avec liens horizontaux : \n");
    testAfficherFichier("data/tests/arbreLienHorizontaux.dat");
    error == OK ? treeDebugPrint(tree) : logError(error);
    treeFree(&tree);
    treeInit(&tree);

    // Un arbre avec que des liens verticaux (liste chaînée)
    error = treeGenerateFromFile("data/tests/arbreLienVerticaux.dat", &tree);
    printf("Arbre avec liens verticaux : \n");
    testAfficherFichier("data/tests/arbreLienVerticaux.dat");
    error == OK ? treeDebugPrint(tree) : logError(error);
    treeFree(&tree);
    treeInit(&tree);

    // Un arbre avec des liens horizontaux et verticaux
    error = treeGenerateFromFile("data/tests/arbreLienHorizontauxVerticaux.dat", &tree);
    printf("Arbre avec liens verticaux : \n");
    testAfficherFichier("data/tests/arbreLienHorizontauxVerticaux.dat");
    error == OK ? treeDebugPrint(tree) : logError(error);
    treeFree(&tree);
}

/**
 * @brief Test de l'affichage d'arbres
 *
 */
void testAffichage(void)
{
    tree_t tree;
    error_t error;
    treeInit(&tree);

    printf(GRE "\n\n\n============= Tests affichage =============\n" RESET);

    // Un arbre vide
    error = treeGenerateFromFile("data/tests/arbreVide.dat", &tree);
    printf("Arbre vide : \n");
    testAfficherFichier("data/tests/arbreVide.dat");

    error == OK ? treePrint(tree) : logError(error);
    treeFree(&tree);
    treeInit(&tree);

    // Un arbre avec une seule lettre
    error = treeGenerateFromFile("data/tests/arbreUneSeuleLettre.dat", &tree);
    printf("Arbre avec une seule lettre : \n");
    testAfficherFichier("data/tests/arbreUneSeuleLettre.dat");

    error == OK ? treePrint(tree) : logError(error);
    treeFree(&tree);
    treeInit(&tree);

    // Un arbre avec que des liens horizontaux (liste chaînée)
    error = treeGenerateFromFile("data/tests/arbreLienHorizontaux.dat", &tree);
    printf("Arbre avec liens horizontaux : \n");
    testAfficherFichier("data/tests/arbreLienHorizontaux.dat");
    error == OK ? treePrint(tree) : logError(error);
    treeFree(&tree);
    treeInit(&tree);

    // Un arbre avec que des liens verticaux (liste chaînée)
    error = treeGenerateFromFile("data/tests/arbreLienVerticaux.dat", &tree);
    printf("Arbre avec liens verticaux : \n");
    testAfficherFichier("data/tests/arbreLienVerticaux.dat");
    error == OK ? treePrint(tree) : logError(error);
    treeFree(&tree);
    treeInit(&tree);

    // Un arbre avec des liens horizontaux et verticaux
    error = treeGenerateFromFile("data/tests/arbreLienHorizontauxVerticaux.dat", &tree);
    printf("Arbre avec liens verticaux : \n");
    testAfficherFichier("data/tests/arbreLienHorizontauxVerticaux.dat");
    error == OK ? treePrint(tree) : logError(error);
    treeFree(&tree);
}

/**
 * @brief Test d'insertion de mots dans un arbre
 *
 */
void testInsertion(void)
{
    tree_t tree;
    error_t error;
    treeInit(&tree);

    printf(GRE "\n\n\n============= Tests d'insertion =============\n" RESET);

    //Ajout d'un mot dans un arbre vide
    error = treeGenerateFromFile("data/tests/arbreVide.dat", &tree);
    printf("Arbre vide : \n");
    testAfficherFichier("data/tests/arbreVide.dat");
    error == OK ? treePrint(tree) : logError(error);
    treeAddWord(&tree, "arbre");
    printf("Nouvel arbre : \n");
    error == OK ? treePrint(tree) : logError(error);
    treeFree(&tree);
    treeInit(&tree);

    //Ajout d'un mot dans un arbre avec une seule lettre
    error = treeGenerateFromFile("data/tests/arbreUneSeuleLettre.dat", &tree);
    printf("Arbre avec une seule lettre : \n");
    testAfficherFichier("data/tests/arbreUneSeuleLettre.dat");
    error == OK ? treePrint(tree) : logError(error);
    treeAddWord(&tree, "branche");
    printf("Nouvel arbre : \n");
    error == OK ? treePrint(tree) : logError(error);
    treeFree(&tree);
    treeInit(&tree);

    //Ajout d'un mot dans un arbre avec une seule lettre qui est la première lettre du mot
    error = treeGenerateFromFile("data/tests/arbreUneSeuleLettre.dat", &tree);
    printf("Arbre avec une seule lettre qui est la première lettre du mot : \n");
    testAfficherFichier("data/tests/arbreUneSeuleLettre.dat");
    error == OK ? treePrint(tree) : logError(error);
    treeAddWord(&tree, "arbre");
    printf("Nouvel arbre : \n");
    error == OK ? treePrint(tree) : logError(error);
    treeFree(&tree);
    treeInit(&tree);

    //Ajout d'un mot dans un arbre avec que des liens horizontaux (liste chaînée)
    error = treeGenerateFromFile("data/tests/arbreLienHorizontaux.dat", &tree);
    testAfficherFichier("data/tests/arbreLienHorizontaux.dat");
    printf("Arbre avec liens horizontaux : \n");
    error == OK ? treePrint(tree) : logError(error);
    treeAddWord(&tree, "dictionnaire");
    printf("Nouvel arbre : \n");
    error == OK ? treePrint(tree) : logError(error);
    treeFree(&tree);
    treeInit(&tree);

    //Un arbre avec que des liens verticaux (liste chaînée)
    error = treeGenerateFromFile("data/tests/arbreLienVerticaux.dat", &tree);
    testAfficherFichier("data/tests/arbreLienVerticaux.dat");
    printf("Arbre avec liens verticaux : \n");
    error == OK ? treePrint(tree) : logError(error);
    treeAddWord(&tree, "bonjour");
    printf("Nouvel arbre : \n");
    error == OK ? treePrint(tree) : logError(error);
    treeFree(&tree);
    treeInit(&tree);

    //Ajout d'un mot déjà présent dans un arbre
    error = treeGenerateFromFile("data/tests/arbreLienHorizontauxVerticaux.dat", &tree);
    testAfficherFichier("data/tests/arbreLienHorizontauxVerticaux.dat");
    printf("Arbre avec mot déjà présent : \n");
    error == OK ? treePrint(tree) : logError(error);
    treeAddWord(&tree, "aviateur");
    printf("Nouvel arbre : \n");
    error == OK ? treePrint(tree) : logError(error);
    treeFree(&tree);
    treeInit(&tree);

    //Ajout d'un sous mot déjà présent dans un arbre
    error = treeGenerateFromFile("data/tests/arbreLienHorizontauxVerticaux2.dat", &tree);
    testAfficherFichier("data/tests/arbreLienHorizontauxVerticaux2.dat");
    printf("Arbre avec le mot \"bijou\" : \n");
    error == OK ? treePrint(tree) : logError(error);
    treeAddWord(&tree, "bijouterie");
    printf("Nouvel arbre : \n");
    error == OK ? treePrint(tree) : logError(error);
    treeFree(&tree);
    treeInit(&tree);

    //Ajout d’un mot ayant un sous-mot déjà présent dans l’arbre
    error = treeGenerateFromFile("data/tests/arbreLienHorizontauxVerticaux3.dat", &tree);
    testAfficherFichier("data/tests/arbreLienHorizontauxVerticaux3.dat");
    printf("Arbre avec le mot \"bijouterie\" : \n");
    error == OK ? treePrint(tree) : logError(error);
    treeAddWord(&tree, "bijou");
    printf("Nouvel arbre : \n");
    error == OK ? treePrint(tree) : logError(error);
    treeFree(&tree);
    treeInit(&tree);

    //Ajout d’un mot donné en majuscules à la fonction
    error = treeGenerateFromFile("data/tests/arbreLienHorizontauxVerticaux.dat", &tree);
    testAfficherFichier("data/tests/arbreLienHorizontauxVerticaux.dat");
    printf("Ajout d'un mot donné en majuscules à la fonction : \n");
    error == OK ? treePrint(tree) : logError(error);
    treeAddWord(&tree, "ARTICLE");
    printf("Nouvel arbre : \n");
    error == OK ? treePrint(tree) : logError(error);
    treeFree(&tree);
    treeInit(&tree);

    //Ajout d’un mot vide
    error = treeGenerateFromFile("data/tests/arbreVide.dat", &tree);
    testAfficherFichier("data/tests/arbreVide.dat");
    printf("Arbre vide - Ajout d'un mot vide : \n");
    error == OK ? treeDebugPrint(tree) : logError(error);
    treeAddWord(&tree, "");
    printf("Nouvel arbre : \n");
    error == OK ? treeDebugPrint(tree) : logError(error);
    treeFree(&tree);
    treeInit(&tree);

    //Ajout d’un mot constitué d’espaces
    error = treeGenerateFromFile("data/tests/arbreVide.dat", &tree);
    testAfficherFichier("data/tests/arbreVide.dat");
    printf("Arbre vide - Ajout d'un mot constitué d'espaces : \n");
    error == OK ? treeDebugPrint(tree) : logError(error);
    treeAddWord(&tree, "          ");
    printf("Nouvel arbre : \n");
    error == OK ? treeDebugPrint(tree) : logError(error);
    treeFree(&tree);
    treeInit(&tree);

    //Ajout d’un mot composé
    error = treeGenerateFromFile("data/tests/arbreVide.dat", &tree);
    testAfficherFichier("data/tests/arbreVide.dat");
    printf("Ajout d'un mot-composé : \n");
    error == OK ? treeDebugPrint(tree) : logError(error);
    treeAddWord(&tree, "casse-tete");
    printf("Nouvel arbre : \n");
    error == OK ? treeDebugPrint(tree) : logError(error);

    treeFree(&tree);
}

/**
 * @brief Test de recherche de motif de mot
 * 
 */
void testRechercheMotif(void)
{
    tree_t tree;
    char filename[] = "data/tests/arbreLienHorizontauxVerticaux.dat";
    treeInit(&tree);

    printf(GRE "\n\n\n============= Tests recherches =============\n" RESET);

    //Recherche d’un motif vide
    treeGenerateFromFile(filename, &tree);
    printf("Recherche du motif \"\" : \n");
    testAfficherFichier(filename);
    treeSearchMotif(&tree, "");
    printf("\n");
    treeFree(&tree);

    //Recherche d’un motif présent dans un seul mot dans l’arbre
    treeGenerateFromFile(filename, &tree);
    printf("Recherche du motif \"" BLU "AVIO" RESET "\" : \n");
    testAfficherFichier(filename);
    treeSearchMotif(&tree, "avio");
    printf("\n");
    treeFree(&tree);

    //Recherche d’un motif présent dans plusieurs mots dans l’arbre
    treeGenerateFromFile(filename, &tree);
    printf("Recherche du motif \"" BLU "AVI" RESET "\" : \n");
    testAfficherFichier(filename);
    treeSearchMotif(&tree, "avi");
    printf("\n");
    treeFree(&tree);

    //Recherche d’un motif pas présent dans l’arbre
    treeGenerateFromFile(filename, &tree);
    printf("Recherche du motif \"" BLU "BRI" RESET "\" : \n");
    testAfficherFichier(filename);
    treeSearchMotif(&tree, "bri");
    printf("\n");
    treeFree(&tree);
}

/**
 * @brief Lance tous les tests
 *
 */
void testRun(void)
{
    printf("\n\n=============== Lancement des tests ===============\n");
    testFormeAlgebrique();
    testAffichage();
    testInsertion();
    printf("\n\n=============== Fin des tests ===============\n");
}

/**
 * @brief Affiche un ficher text, pour afficher la forme algébrique
 * 
 * @param filename Nom du fichier
 */
void testAfficherFichier(char *filename)
{
    FILE *flot = fopen(filename, "r");
    char tmp;
    if (flot)
    {
        printf(GRE "Fichier : %s\n" YEL, filename);
        fscanf(flot, "%c", &tmp);
        while (!feof(flot))
        {
            printf("%c", tmp);
            fscanf(flot, "%c", &tmp);
        }
        printf("\n\n" RESET);
        fclose(flot);
    }
    else
    {
        logError(FILE_NOT_FOUND);
    }
}

/**
 * @brief Un arbre simple pour une visualisation avec DDD
 * 
 */
void testDDD(void)
{

    tree_t tree;
    treeInit(&tree);
    treeGenerateFromFile("data/tests/arbreLienHorizontauxVerticaux2.dat", &tree);

    treeDebugPrint(tree);

    treePrint(tree);
    treeFree(&tree);
}