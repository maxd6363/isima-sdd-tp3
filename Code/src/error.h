/**
 * @brief      Définition du type erreur et des fonctions associées
 *
 * @author     Maxime POULAIN - Lilian HERTEL
 * @date       2021
 */

#ifndef ERROR_H
#define ERROR_H

/**
 * Structure erreur
 */
typedef enum {
	OK,
	ERROR,
	FILE_NOT_FOUND,
	ALLOC_ERROR,
	OUT_OF_BOUNDS,
	NULL_POINTER,
	NOT_IMPLEMENTED_YET,
	STACK_OVERFLOW,
	STACK_EMPTY,
	QUEUE_OVERFLOW,
	QUEUE_EMPTY
} error_t;

/**
 * @brief      Fonction pour logger les erreurs
 *
 * @param[in]  err   L'erreur
 */
void logError(error_t err);

#endif